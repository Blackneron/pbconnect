# PBconnect Scripts - README #
---

### Overview ###

The **PBconnect** scripts (Bash) **ping_test.sh** and **wget_test.sh** are made for testing an Internet connection in two different ways (ping host / wget connections to websites) and to write the results into a logfile which at the end of every day (at midnight) is sent to the administrator by email. The script **send_ip.sh** checks if the LAN IP address has changed (for accessing a headless Raspberry Pi which has received his IP address from a DHCP server) and sends an email with the IP address to the administrator.


### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**.
* Copy the scripts **ping_test.sh**, **wget_test.sh** and **send_ip.sh** to your computer.
* Make the scripts executable: **chmod +x ping_test.sh wget_test.sh send_ip.sh**.
* Edit the configuration part of the three scripts, especially the email account credentials.
* Open the firewall port needed for sending emails (**587**).
* Start the scripts from the command prompt (or a cronjob to start them with every reboot) like: **./ping_test.sh**.
* Check (with top) if the scripts are running and the logfiles are sent to the administrator by email.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **FolderBackup** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
