#!/bin/bash


########################################################################
#                                                                      #
#                            S E N D - I P                             #
#                                                                      #
#           SENDS THE ACTUAL IP ADDRESS TO AN EMAIL ADDRESS            #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.1 / 27.09.2017                       #
#                                                                      #
########################################################################
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendemail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================
#
# Check that the following firewall port is open for sending emails.
#
#   Port 587 - Simple Mail Transfer Protocol (SMTP) => sending emails
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the hostname or IP address for testing.
# ======================================================================
HOSTNAME="8.8.4.4" # Google DNS-Server


# ======================================================================
# Specify if there should be a startup delay (in seconds).
# ======================================================================
STARTUP_DELAY=20


# ======================================================================
# Specify the wait time between the connection checks (in seconds).
# ======================================================================
WAIT_TIME=60


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
EMAIL_USERNAME="john.doe@gmail.com"
EMAIL_PASSWORD="secret_pass"
EMAIL_HOSTNAME="smtp.gmail.com"
EMAIL_PORT="587"
EMAIL_TLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
EMAIL_SENDER="admin@localhost"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  EMAIL_RECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
EMAIL_RECEIVER="john@example.com"


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Check if a startup delay is needed.
# ======================================================================
if [ "$STARTUP_DELAY" -gt "0" ]; then


  # ====================================================================
  # Sleep some time.
  # ====================================================================
  sleep $STARTUP_DELAY
fi


# ======================================================================
# Get the paths of the necessary executables.
# ======================================================================
SENDEMAIL="$(which sendEmail)"


# ======================================================================
# Get the actual hostname where the script is running.
# ======================================================================
SCRIPT_HOST="$(hostname)"


# ======================================================================
# Initialize the old IP address.
# ======================================================================
IP_ADDRESS_OLD="UNKNOWN"


# ======================================================================
# Loop until the end of the world.
# ======================================================================
while [ 1 ]; do


  # ====================================================================
  # Ping the host.
  # ====================================================================
  ping -c 1 -W 0.7 $HOSTNAME > /dev/null 2>&1


  # ====================================================================
  # Check if the ping to the host was successful.
  # ====================================================================
  if [ $? -eq 0 ] ; then


    # ==================================================================
    # Get the actual IP address.
    # ==================================================================
    IP_ADDRESS=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/')


    # ==================================================================
    # Check if the IP address has changed or was unknown before.
    # ==================================================================
    if [ "$IP_ADDRESS" != "$IP_ADDRESS_OLD" ]; then


      # ================================================================
      # Save the actual IP address for later comparison.
      # ================================================================
      IP_ADDRESS_OLD=$IP_ADDRESS


      # ================================================================
      # Specify the actual date in the yyyy-mm-dd format.
      # ================================================================
      E_DATE="$(date +"%d.%m.%Y")"


      # ================================================================
      # Specify the actual time in the hh-mm-ss format.
      # ================================================================
      E_TIME="$(date +"%H:%M:%S")"


      # ================================================================
      # Specify more email details.
      # ================================================================
      EMAIL_SUBJECT="$E_DATE / $E_TIME | Host: $SCRIPT_HOST | IP: $IP_ADDRESS"
      EMAIL_MESSAGE="Date: $E_DATE\nTime: $E_TIME\nHostname: $SCRIPT_HOST\nIP address: $IP_ADDRESS"


      # ================================================================
      # Send the IP address via email.
      # ================================================================
      $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8
    fi
  fi


  # ====================================================================
  # Sleep some time.
  # ====================================================================
  sleep $WAIT_TIME
done
